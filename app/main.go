package main

import (
	"gitlab.com/devops_igor/geekbrains_microservices/app/app"
	"gitlab.com/devops_igor/geekbrains_microservices/app/config"
)

func main() {
	config := config.GetConfig()

	app := &app.App{}
	app.Initialize(config)
	app.Run(":8000")
}
