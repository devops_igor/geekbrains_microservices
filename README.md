# Geekbrains_microservices
Homework for lesson 8 of GB microservices. Minikube as k8s cluster.

# Useful commands
Create namespace for gitlab-runner
```bash
kubectl create ns gitlab
```
In ./gitlab-runner/gitlab-runner.yaml find ```<CHANGE ME>``` and change it to registration token
```yaml
stringData:
  runner-registration-token: <CHANGE ME>
```
Apply gitlab-runner manifest
```bash
kubectl apply --namespace gitlab -f ./gitlab-runner/gitlab-runner.yaml
```
Create namespace for stage environment
```bash
kubectl create ns stage
```
Create namespace for prod environment
```bash
kubectl create ns prod
```
Create deploy service account for stage environment
```bash
kubectl create sa deploy --namespace stage
```
Create deploy service account for prod environment
```bash
kubectl create sa deploy --namespace prod
```
Create rolebinding for stage
```bash
kubectl create rolebinding deploy --serviceaccount stage:deploy --clusterrole edit --namespace stage
```
Create rolebinding for prod
```bash
kubectl create rolebinding deploy --serviceaccount prod:deploy --clusterrole edit --namespace prod
```
Get a token for stage which will be ```K8S_STAGE_CI_TOKEN``` in CI/CD variables
```bash
export NAMESPACE=stage
kubectl get secret $(kubectl get sa deploy --namespace $NAMESPACE -o jsonpath='{.secrets[0].name}') --namespace $NAMESPACE -o jsonpath='{.data.token}'
```
Get a token for prod which will be ```K8S_PROD_CI_TOKEN``` in CI/CD variables
```bash
export NAMESPACE=prod
kubectl get secret $(kubectl get sa deploy --namespace $NAMESPACE -o jsonpath='{.secrets[0].name}') --namespace $NAMESPACE -o jsonpath='{.data.token}'
```
Create secret for deploy token from ```Settings-Repository-Deploy tokens``` for stage
```bash
kubectl create secret docker-registry gitlab-registry --docker-server=registry.gitlab.com --docker-username=geekbrains --docker-password=<TOKEN> --docker-email=admin@admin.admin --namespace stage
```
Create secret for deploy token from ```Settings-Repository-Deploy tokens``` for prod
```bash
kubectl create secret docker-registry gitlab-registry --docker-server=registry.gitlab.com --docker-username=geekbrains --docker-password=<TOKEN> --docker-email=admin@admin.admin --namespace prod
```
Apply ingress
```bash
kubectl apply --namespace stage -f ./manifest/ingress.yaml
```
